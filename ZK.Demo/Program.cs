﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace ZK.Demo
{
    class MainClass
    {      
        public static void Main(string[] args)
        {
			Run(new IPEndPoint(IPAddress.Parse(args[0]), int.Parse(args[1]))).Wait();
        }

		private static async Task Run(EndPoint endpoint)
		{
			using (var device = new Device(endpoint, 0))
			{
				try
				{
					await device.Connect();
				}
				catch (Unauthrized)
				{
					Console.WriteLine("Device connected");
					await device.EnableDevice();
					Console.WriteLine("Device enabled");
					var sn = await device.GetSerialNumber();
					Console.WriteLine("Device SerialNumber {0}", sn);
                    await device.RegisterEvent();
					Console.WriteLine("Device event registred");
				}            
			}
		}      
    }
}

﻿using System;
using System.Net.Sockets;
using System.Threading.Tasks;
using NUnit.Framework;
using static ZK.Test.Helper;

namespace ZK.Test
{
    [TestFixture]
    public class MainTests
    {
        [Test]
        public async Task ConnectTest()
        {
            var factory = new Stubs.Factory();
            var socket = new Stubs.Socket();
            var endpoint = new Stubs.EndPoint();

            factory.ExpectsCreateSocket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp, socket);

            socket.ExpectsSendToAndReceiveFrom(
                endpoint,
                Unhex("E80306EB11110000"),
                Unhex("D507000000FF0000")
            );

            socket.ExpectsSendToAndReceiveFrom(
                endpoint,
                Unhex("EA0313FD00FF0100"),
                Unhex("D007000000FF0100")
            );

            socket.ExpectsSendToAndReceiveFrom(
                endpoint,
                Unhex("0B00119F00FF02007e6162"),
                Unhex("D007000000FF02007e62633d636400")
            );

            socket.ExpectsSendToAndReceiveFrom(
                endpoint,
                Unhex("0B00EDB700FF03007e53657269616c4e756d626572"),
                Unhex("D007000000FF03007e53657269616c4e756d6265723d61626331323300")
            );

            socket.ExpectsSendToAndReceiveFrom(
                endpoint,
                Unhex("F40105FF00FF04000100"),
                Unhex("D007000000FF0400")
            );

            socket.ExpectsReceiveFrom(
                endpoint,
                Unhex("F401000000000000313233343536000000000000")
            );

            socket.ExpectsEndlessReceiveFrom(endpoint);

            socket.ExpectsSocketDispose();

            var device = new Device(factory, endpoint, 0x1111);

            ushort session = 0;
            device.OnSessionChange += newsession => session = newsession;

            device.OnAttendance += (Attendance attendance) => {
                Assert.AreEqual(0xFF00, session);
                Assert.AreEqual(123456, attendance.User);
                device.Dispose();
            };

            Assert.Throws<Unauthrized>(async () => await device.Connect());   

            await device.EnableDevice();

            Assert.AreEqual("cd", await device.GetConfigurationParameter("ab"));

            Assert.AreEqual("abc123", await device.GetSerialNumber());

            await device.RegisterEvent();
            

            factory.Verify();
            socket.Verify();
        }
    }
}
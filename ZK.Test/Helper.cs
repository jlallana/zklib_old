﻿using System;
using System.Net;
using System.Net.Sockets;
using NUnit.Framework;

namespace ZK.Test
{
    public static class Helper
    {      
        public static void ExpectsSendToAndReceiveFrom(this Stubs.Socket socket, EndPoint expectedEp, byte[] send, byte[] receive)
        {
            AsyncCallback unlock = default(AsyncCallback);
            var sendResult = new Stubs.AsyncResult();
            var receiveResult = new Stubs.AsyncResult();

            socket.BeginReceiveFrom += (buffer, offset, size, flags, endpoint, callback, state) =>
            {
				Assert.AreSame(expectedEp, endpoint);
                Assert.AreEqual(0, offset);
                Assert.AreEqual(buffer.Length, size);
                Array.Copy(receive, 0, buffer, 0, receive.Length);
                unlock = callback;
                return receiveResult;
            };

            socket.BeginSendTo += (buffer, offset, size, flags, endpoint, callback, state) =>
            {
				Assert.AreSame(expectedEp, endpoint);
                Assert.AreEqual(send, buffer);
                Assert.AreEqual(0, offset);
                Assert.AreEqual(send.Length, size);
                Assert.AreEqual(SocketFlags.None, flags);
                Assert.IsNull(state);
                callback(sendResult);
                return default(IAsyncResult);
            };

            socket.EndSendTo += (result) =>
            {
                unlock(receiveResult);
                Assert.AreSame(sendResult, result);
                return default(int);
            };

            socket.EndReceiveFrom += (result, endpoint) =>
            {
				Assert.AreSame(expectedEp, endpoint);
                Assert.AreSame(receiveResult, result);
				return receive.Length;
            };
        }

        public static void ExpectsReceiveFrom(this Stubs.Socket socket, EndPoint expectedEp, byte[] receive)
        {
            var asyncResult = new Stubs.AsyncResult();
            socket.BeginReceiveFrom += (buffer, offset, size, flags, endpoint, callback, state) =>
            {
                Assert.AreSame(expectedEp, endpoint);
                Assert.AreEqual(0, offset);
                Assert.AreEqual(buffer.Length, size);
                Array.Copy(receive, 0, buffer, 0, receive.Length);
                callback(asyncResult);
                return asyncResult;
            };

            socket.EndReceiveFrom += (result, endpoint) =>
            {
                Assert.AreSame(expectedEp, endpoint);
                Assert.AreSame(asyncResult, result);
                return receive.Length;
            };
        }

		public static void ExpectsEndlessReceiveFrom(this Stubs.Socket socket, EndPoint expectedEp)
        {
            socket.BeginReceiveFrom += (buffer, offset, size, flags, endpoint, callback, state) =>
            {
                Assert.AreSame(expectedEp, endpoint);
                return new Stubs.AsyncResult();
            };
        }

        public static void ExpectsSocketDispose(this Stubs.Socket socket)
        {
            socket.Dispose += () => { };
        }

        public static void ExpectsCreateSocket(this Stubs.Factory factory, AddressFamily expectedAddressFamily, SocketType expectedSocketType, ProtocolType expectedProtocolType, ISocket returnSocket)
        {
            factory.CreateSocket += (addressFamily, socketType, protocolType) =>
            {
                Assert.AreEqual(expectedAddressFamily, addressFamily);
                Assert.AreEqual(expectedSocketType, socketType);
                Assert.AreEqual(expectedProtocolType, protocolType);
                return returnSocket;
            };
        }

        public static byte[] Unhex(string values)
        {
            var result = new byte[values.Length / 2];
            for (var i = 0; i < result.Length; i++)
            {
                result[i] = Convert.ToByte(values.Substring(i * 2, 2), 16);
            }
            return result;
        }
    }
}

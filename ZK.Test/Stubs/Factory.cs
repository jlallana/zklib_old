﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace ZK.Test.Stubs
{
	public class Factory : IFactory
	{
		Queue<Func<AddressFamily, SocketType, ProtocolType, ISocket>> createSocket = new Queue<Func<AddressFamily, SocketType, ProtocolType, ISocket>>();

		ISocket IFactory.CreateSocket(AddressFamily addressFamily, SocketType socketType, ProtocolType protocolType)
		{
			return createSocket.Dequeue().Invoke(addressFamily, socketType, protocolType);
		}

		public event Func<AddressFamily, SocketType, ProtocolType, ISocket> CreateSocket
		{
			add
			{
				createSocket.Enqueue(value);
			}
			remove
			{
				throw new NotSupportedException();
			}
		}

		public void Verify()
		{
			if (createSocket.Count > 0)
			{
				throw new Exception("Missing expectations");
			}
		}
	}
}

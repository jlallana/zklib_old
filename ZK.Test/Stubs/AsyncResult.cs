﻿using System;
using System.Threading;

namespace ZK.Test.Stubs
{
	public class AsyncResult : IAsyncResult
	{
		object IAsyncResult.AsyncState => throw new NotImplementedException();

		WaitHandle IAsyncResult.AsyncWaitHandle => throw new NotImplementedException();

		bool IAsyncResult.CompletedSynchronously => throw new NotImplementedException();

		bool IAsyncResult.IsCompleted => throw new NotImplementedException();
	}
}
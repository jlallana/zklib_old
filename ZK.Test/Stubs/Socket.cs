﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace ZK.Test.Stubs
{
	public class Socket : ISocket, IDisposable
	{
		Queue<Action> DisposeQueue = new Queue<Action>();

		void IDisposable.Dispose()
        {
			DisposeQueue.Dequeue().Invoke();
        }

        public event Action Dispose
        {
            add
            {
                DisposeQueue.Enqueue(value);
            }
            remove
            {
                throw new NotSupportedException();
            }
        }

		Queue<Func<byte[], int, int, SocketFlags, System.Net.EndPoint, AsyncCallback, object, IAsyncResult>> BeginReceiveFromQueue = new Queue<Func<byte[], int, int, SocketFlags, System.Net.EndPoint, AsyncCallback, object, IAsyncResult>>();
      
		IAsyncResult ISocket.BeginReceiveFrom(byte[] buffer, int offset, int size, SocketFlags flags, ref System.Net.EndPoint endpoint, AsyncCallback callback, object state)
		{
			return BeginReceiveFromQueue.Dequeue().Invoke(buffer, offset, size, flags, endpoint, callback, state);
		}

		public event Func<byte[], int, int, SocketFlags, System.Net.EndPoint, AsyncCallback, object, IAsyncResult> BeginReceiveFrom
		{
			add
			{
				BeginReceiveFromQueue.Enqueue(value);
			}
			remove
			{
				throw new NotImplementedException();
			}
		}

       
		Queue<Func<byte[], int, int, SocketFlags, System.Net.EndPoint, AsyncCallback, object, IAsyncResult>> BeginSendToQueue = new Queue<Func<byte[], int, int, SocketFlags, System.Net.EndPoint, AsyncCallback, object, IAsyncResult>>();

		IAsyncResult ISocket.BeginSendTo(byte[] buffer, int offset, int size, SocketFlags flags, System.Net.EndPoint endpoint, AsyncCallback callback, object state)
        {
            return BeginSendToQueue.Dequeue().Invoke(buffer, offset, size, flags, endpoint, callback, state);
        }

		public event Func<byte[], int, int, SocketFlags, System.Net.EndPoint, AsyncCallback, object, IAsyncResult> BeginSendTo
        {
            add
            {
                BeginSendToQueue.Enqueue(value);
            }
            remove
            {
                throw new NotImplementedException();
            }
        }

		Queue<Func<IAsyncResult, int>> EndSendToQueue = new Queue<Func<IAsyncResult, int>>();

        int ISocket.EndSendTo(IAsyncResult asyncResult)
        {
            return EndSendToQueue.Dequeue().Invoke(asyncResult);
        }
        
        public event Func<IAsyncResult, int> EndSendTo
        {
            add
            {
                EndSendToQueue.Enqueue(value);
            }
            remove
            {
                throw new NotSupportedException();
            }
        }

		Queue<Func<IAsyncResult, System.Net.EndPoint, int>> EndReceiveFromQueue = new Queue<Func<IAsyncResult, System.Net.EndPoint, int>>();

		int ISocket.EndReceiveFrom(IAsyncResult asyncResult, ref System.Net.EndPoint endpoint)
        {
			return EndReceiveFromQueue.Dequeue().Invoke(asyncResult, endpoint);
        }

		public event Func<IAsyncResult, System.Net.EndPoint, int> EndReceiveFrom
        {
            add
            {
				EndReceiveFromQueue.Enqueue(value);
            }
            remove
            {
                throw new NotSupportedException();
            }
        }
      
        public void Verify()
        {
            if (BeginReceiveFromQueue.Count > 0 || BeginSendToQueue.Count > 0 || EndReceiveFromQueue.Count > 0 || EndSendToQueue.Count > 0 || DisposeQueue.Count > 0)
            {
                throw new Exception("Missing call expectations");
            }
        }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ZK
{
	public class Device : IDisposable
	{
		enum Header : ushort
		{
			CMD_CONNECT = 1000,
			CMD_ENABLEDEVICE = 1002,
			CMD_REG_EVENT = 500,
			CMD_ACK_OK = 2000,
			CMD_ACK_UNAUTH = 2005,
            CMD_OPTIONS_PRQ = 11
		}

		private object SendLock = new object();

		Dictionary<ushort, Action<Header, byte[]>> callbacks = new Dictionary<ushort, Action<Header, byte[]>>();

        public event Action<ushort> OnSessionChange;

		ushort session = 0;
		ushort sequence = 0;

		ISocket socket;
		EndPoint endpoint;

        public Device(IFactory factory, EndPoint endPoint, ushort session = 0)
		{
            this.session = session;
			this.endpoint = endPoint;
			socket = factory.CreateSocket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			ReceiveNext();
		}

        public Device(EndPoint endPoint, ushort session = 0) : this(new Factory(), endPoint, session)
		{

		}

        public event Action<Attendance> OnAttendance;

		void ReceiveNext()
		{
			var buffer = new byte[1024];

			socket.BeginReceiveFrom(buffer, 0, buffer.Length, default(SocketFlags), ref endpoint, (result) =>
			{
                var received = socket.EndReceiveFrom(result, ref endpoint);
                var header = (Header)BitConverter.ToUInt16(buffer, 0);
                var resultData = new byte[received - 8];
                Array.Copy(buffer, 8, resultData, 0, resultData.Length);

                if(header != Header.CMD_REG_EVENT)
                {
                    this.session = BitConverter.ToUInt16(buffer, 4);
                    if (this.OnSessionChange != null)
                    {
                        this.OnSessionChange(this.session);
                    }
                }

				ReceiveNext();

                if(header == Header.CMD_REG_EVENT)
                {
                    this.OnAttendance(new Attendance(resultData));
                }
                else
                {
                    var currentSequence = BitConverter.ToUInt16(buffer, 6);
                    var callback = callbacks[currentSequence];
                    callbacks.Remove(currentSequence);
                    callback.Invoke((Header)BitConverter.ToUInt16(buffer, 0), resultData);
                }
			}, null);
		}

		public void Dispose()
		{
			socket.Dispose();
		}

		public async Task Connect()
        {
			await ExecuteCommandAsync(Header.CMD_CONNECT, new byte[0]);
        }      

		public async Task EnableDevice()
        {
			await ExecuteCommandAsync(Header.CMD_ENABLEDEVICE, new byte[0]);
        }

		public async Task RegisterEvent()
		{
			await ExecuteCommandAsync(Header.CMD_REG_EVENT, new byte[] { 0x01, 0x00 });
		}
      
		public async Task<string> GetConfigurationParameter(string attribute)
		{
			var data = await ExecuteCommandAsync(Header.CMD_OPTIONS_PRQ, Encoding.ASCII.GetBytes("~" + attribute));
			return Encoding.ASCII.GetString(data).Substring(attribute.Length + 2).TrimEnd(new[] { '\0' });
		}

		public Task<string> GetSerialNumber()
		{
			return GetConfigurationParameter("SerialNumber");
		}

		Task<byte[]> ExecuteCommandAsync(Header header, byte[] data)
		{
			var task = new TaskCompletionSource<byte[]>();

			ExecuteCommand(header, data, (status, result) => {
				if (status == Header.CMD_ACK_OK)
                {
                    task.SetResult(result);
                }
                else
                {
                    task.SetException(new Unauthrized());
                }
			});
            
            return task.Task;
		}

		void ExecuteCommand(Header header, byte[] arguments, Action<Header, byte[]> callback)
        {
			ushort currentSequence = 0;

			lock (SendLock)
			{
				currentSequence = this.sequence++;
                callbacks.Add(currentSequence, callback);
			}
         
            var buffer = new byte[8 + arguments.Length];
            buffer.SetUInt16((ushort)header, 0);
            buffer.SetUInt16(session, 4);
            buffer.SetUInt16(currentSequence, 6);

            Array.Copy(arguments, 0, buffer, 8, arguments.Length);
            buffer.SetUInt16(GenerateChecksum(buffer), 2);

            socket.BeginSendTo(buffer, 0, buffer.Length, SocketFlags.None, endpoint, (result) =>
            {
                socket.EndSendTo(result);
            }, null);

        }

        ushort GenerateChecksum(byte[] buffer)
        {
            uint checksum = 0;
            for (var i = 0; i < buffer.Length; i += 2)
            {
                if (i == 2)
                {
                    continue;
                }

                if (i + 1 == buffer.Length)
                {
                    checksum += buffer[i];
                }
                else
                {
                    checksum += BitConverter.ToUInt16(buffer, i);
                }

                if (checksum > ushort.MaxValue)
                {
                    checksum -= ushort.MaxValue;
                }
            }
            checksum = ushort.MaxValue - checksum;
            return (ushort)checksum;
        }
    }
}
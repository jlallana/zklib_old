﻿using System;
using System.Net;
using System.Net.Sockets;

namespace ZK
{
	public interface ISocket : IDisposable
    {      
		IAsyncResult BeginSendTo(byte[] buffer, int offset, int size, SocketFlags flags, EndPoint endpoint, AsyncCallback callback, object state);
		int EndSendTo(IAsyncResult asyncResult); 

		IAsyncResult BeginReceiveFrom(byte[] buffer, int offset, int size, SocketFlags flags, ref EndPoint endPoint, AsyncCallback callback, object state);
		int EndReceiveFrom(IAsyncResult asyncResult, ref EndPoint endpoint);
    }
}
                                  
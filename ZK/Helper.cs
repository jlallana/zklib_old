﻿using System;
namespace ZK
{
	static class Helper
	{
		public static void SetUInt16(this byte[] buffer, ushort value, int offset)
		{
			buffer[offset] = (byte)(value & byte.MaxValue);
            buffer[offset + 1] = (byte)(value >> 8);
		}
	}
}

﻿using System;
using System.Net.Sockets;

namespace ZK
{
	public class Socket : System.Net.Sockets.Socket, ISocket
	{
		public Socket(AddressFamily addressFamily, SocketType socketType, ProtocolType protocolType) : base(addressFamily, socketType, protocolType)
		{
		}
	}
}

﻿using System;
using System.Net.Sockets;

namespace ZK
{
    public interface IFactory
    {
		ISocket CreateSocket(AddressFamily addressFamily, SocketType socketType, ProtocolType protocolType);
    }
}

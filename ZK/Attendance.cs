﻿using System;
using System.Text;

namespace ZK
{
    public class Attendance
    {
        internal Attendance(byte[] data)
        {
            this.User = ulong.Parse(Encoding.ASCII.GetString(data, 0, 12).TrimEnd(new[] { '\0' }));
            this.Timestamp = default(DateTime);
        }

        public ulong User { get; private set; }

        public DateTime Timestamp { get; private set; }
    }
}

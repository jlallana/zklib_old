﻿using System;
using System.Net.Sockets;

namespace ZK
{
    class Factory : IFactory
    {
        public ISocket CreateSocket(AddressFamily addressFamily, SocketType socketType, ProtocolType protocolType)
        {
            return new Socket(addressFamily, socketType, protocolType);
        }
    }
}
